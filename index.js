function displayMsgToSelf(){
	console.log('Whatever what happened in the past, keep moving and show to everyone that you can do it after those hard fall and challenges you face.');	
};
/*displayMsgToSelf()
displayMsgToSelf()
displayMsgToSelf()
displayMsgToSelf()
displayMsgToSelf()
displayMsgToSelf()
displayMsgToSelf()
displayMsgToSelf()
displayMsgToSelf()
displayMsgToSelf()*/

let count = 10;

while(count !== 0){
	displayMsgToSelf();
	count--;
};

// While loop will allow us to repeat and action or an instruction as long as the condition is true. \
/*
	1st loop - count 10
	2nd loop - count 9
	3rd loop - count 8
	4th loop - count 7
	5th loop - count 6
	6th loop - count 5
	7th loop - count 4
	8th loop - count 3
	9th loop - count 2
	10th loop - count 1

	while loop checked if count is still NOT equal to 0:
	at this point, before a possible 11th loop, count was decremented to 0 therefore, there was no 11th loop.
	
	If there is no decrementation, the condition is always true, thus, an infinite loop.

	Infinite loop will run your code block forever until you stop it.
	
	An infinite loop is a piece of code that keeps running because a terminating condition is never reached. This may casue your app, browser or even PC to crash.

	Samples:

	while(true){
		console.log("This will result in an infinite loop")
	}

	Always make sure that at the very least your loop condition will be terminated or will be false at one point. 

*/


	// Mini-Activity

	let x = 1;
	while(x<=5){
	console.log(x);
	x++;
};

	// Do-while loop
		//Do while loop is similar to the while loop. However, Do while loop will allow us to run our loop atleast once.

	 	//With the while loop, we check the condition first before running our codeblock, however for do while loop, it will do an instruction first before it will check the condition to run again.

	 	let doWhileCounter = 1;

	 	do{
	 		console.log(doWhileCounter); /*shown on console*/
	 	 }while (doWhileCounter === 0)/*{

	 	 	console.log(doWhileCounter);
	 	 	doWhileCounter--;
	 	 }*/

	 	 // Mini-activity

	 	 let counter = 1;

	 	 do {
	 	 	console.log(counter);
	 	 	counter++;
	 	 } while (counter <= 20)
	 	 

	 	 // For Loop
	 	 	//For loops are much more flexible than while and do-while loop. consists of three parts.
	 	 		 //1. initialization - creating variable which be used as the counter
	 	 		  //2. condition - creating the appropriate condition to run our loop
	 	 		   //3. finalExpression or (decrementation or inrementation) based on your condition
	 		 //Syntax
	 		 /*

			for(initalization; expression/condition; finalExpression)




				
		
	 		 */

	 		 for (let count = 0; count <= 20; count ++){
	 		 	console.log(count);
	 		 };
	 		 // Accessing Array Items
	 		 // Each item in array is ordered acordingly
	 		 // Each item is ordered per index.
	 		 // Each array starts their index at 0.
	 		 let fruits = ["Apple", "Durian", "Kiwi", "Pineapple"];
	 		 // We can also access the items in an array by accessing them through their index.
	 		 // This will allow us to see the 3rd item in our array.
	 		 console.log(fruits[2])
	 		 // Access the 2nd item in the array:
	 		 console.log(fruits[1])

	 		 // Mini-activity:


	 		 console.log(fruits[0]);
	 		 console.log(fruits[3]); 

	 		 // length property is also a property of an array. The .length property in an array shows the total number of items in an array.

	 		 console.log(fruits.length)
	 		 // A more reliable way of checking the last item in an array.
	 		 // arrayName[arrayName.length-1]
	 		 console.log(fruits[fruits.length-1])

	 		 // Show all the items in an array in the console using loops:
	 		 for(let index = 0; index < fruits.length; index++){
	 		 	console.log(fruits[index]);
	 		 };

	 		 // Mini-activity:


	 		 let favoriteCountries = ["Greece","Japan","London","Canada","Australia","South Korea"];

	 		 for (let index = 0; index < favoriteCountries.length-1; index++){

	 		 	console.log(favoriteCountries[index]);
	 			 };


	 		// global/local scope
	 		// Global scoped variables are variables that can be accessed inside a function or anywhere else in our script.
	 		let userName = "super_knight_1000"

	 		function sample() {
	 			// local scoped or function scoped variable
	 			let heroName = "One Punch Man";
	 			console.log(heroName);
	 		}
	 		sample();

	 		// Strings are similar to arrays
	 		// Strings are special comapred to other data types (bool,num) in that it has access to functions and property that some data type do not have.
	 		// Some array methods/functions even work with strings.
	 		let powerpuffGirls = ["Blossom","Bubbles","Buttercup"];
	 		console.log(powerpuffGirls[0]);

	 		let name = "Alexandro";
	 		// You can access every character in a string as you would in an array
	 		console.log(name[0]);
	 		console.log(name[name.length-1]);


	 		// Mini-activity

	 		function displayChar(str){
	 			for(let index = 0; index < str.length; index++){
	 				console.log(str[index])
	 			} 
	 		}

	 		// Delete all spaces in our string

	 		let stringSample1 = "I like New York City."

	 		function deleteSpaces(str){
	 			// local variable, this variable will hold all the letters/characters in our string except for the spaces.
	 			let filteredStr = ""
	 			// Loop over all the charcters in our string and save only characters that are not spaces.
	 			for (let i = 0; i < str.length; i++){
	 				// Check if the current charcter being looped is a spcae or not.
	 				// If the current character is not a space we will save that into our filterStr variable.
	 				if(str[i]!== " "){
	 					filteredStr += str[i]
	 					console.log(filteredStr)
	 				}
	 			}

	 			// return filteredStr;
	 		}

	 		let noSpaceStr = deleteSpaces(stringSample1);
	 		console.log(noSpaceStr);